#!/usr/bin/python3

import csv
import sys

def main():
    fl = sys.argv[1]
    grid = (is_Puzzle(fl))
    print()
    if is_puzzle(grid):
        print('The sudoku is correct!')
    else:
        print('The sudoku is wrong...')

def is_Puzzle(fl):
    with open(sys.argv[1]) as csvfile:
        grid = [list(map(int,rec)) for rec in csv.reader(csvfile,delimiter=',')]
        return grid

def is_puzzle(fl):
    if check_rows(fl) and check_columns(fl) and check_squares(fl):
        return True
    else:
        return False

def compare_all(fl):
    compare = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    return sorted(fl) == compare

def check_rows(fl):
    for i in fl:
        if not compare_all(i):
            return False
    return True

def check_columns(fl):
    for i in range(len(fl)):
        contain = []
        for j in range(len(fl[0])):
            contain.append(fl[j][i])
        if not compare_all(contain):
            return False
    return True

def check_squares(fl):
    for i in range(0, 9, 3):
        for j in range(0, 9, 3):
            nums = fl[i][j:j + 3] + fl[i + 1][j:j + 3] + fl[i + 2][j:j + 3]
            if not compare_all(nums):
                return False
    return True

main()