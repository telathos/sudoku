#!/usr/bin/python3

import csv
import sys
from sudoku import Sudoku 

with open(sys.argv[1]) as csvfile:
    grid = [list(map(int,rec)) for rec in csv.reader(csvfile,delimiter=',')]

print(grid)

puzzle = Sudoku(3,3,board=grid)
# print(puzzle)
puzzle.solve()
x = puzzle.solve()
print(x)

